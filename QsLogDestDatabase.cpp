// Copyright (c) 2013, Razvan Petru
// All rights reserved.

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, this
//   list of conditions and the following disclaimer in the documentation and/or other
//   materials provided with the distribution.
// * The name of the contributors may not be used to endorse or promote products
//   derived from this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

#include "QsLogDestDatabase.h"
#include <QDateTime>
#include <QtGlobal>
#include <iostream>

QsLogging::DatabaseDestination::DatabaseDestination(const QString& connectionName)
{
    if(QSqlDatabase::contains(connectionName)) {
        mDb = QSqlDatabase::database(connectionName, false);
        QString checkDbSql;
        if(mDb.driverName() == "QPSQL" ) {
              checkDbSql = "CREATE TABLE IF NOT EXISTS qslog (\
                id SERIAL PRIMATY KEY, \
                logtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \
                logger TEXT, \
                level TEXT, \
                message TEXT );";
        }
        else if(mDb.driverName() == "QMYSQL" ) {
              checkDbSql = "CREATE TABLE IF NOT EXISTS qslog (\
                id INTEGER PRIMATY KEY AUTO_INCREMENT, \
                logtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \
                logger TEXT, \
                level TEXT, \
                message TEXT );";
        }
        else if(mDb.driverName() == "QSQLITE" ) {
              checkDbSql = "CREATE TABLE IF NOT EXISTS qslog (\
                id INTEGER PRIMARY KEY AUTOINCREMENT, \
                logtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \
                logger TEXT, \
                level TEXT, \
                message TEXT );";
        }
        else {
            std::cerr << "QsLog: only PostgreSQL, MySQL and SQLite is supported." << "\n";
            return;
        }
        if (!mDb.open()) {
            std::cerr << "QsLog: unable open or create database: " <<  mDb.lastError().text().toUtf8().constData() << "\n";
            return;
        }
        QSqlQuery tableQuery(mDb);
        if(!tableQuery.exec(checkDbSql)) {
            std::cerr << "QsLog: unable to create log table: " << tableQuery.lastError().text().toUtf8().constData() << "\n";
            return;
        }
    }
    else {
        std::cerr << "QsLog: log database " << qPrintable(connectionName) << " not exist." << "\n";
    }
}

void QsLogging::DatabaseDestination::write(const QString& message, Level level, const QString &sender)
{
   if(isValid()) {
      const char* const levelName = QsLogging::LevelToText(level);
      QString sql = QString("INSERT INTO qslog(logger, level, message) VALUES (?, ?, ?)");
      QSqlQuery insertQuery(mDb);
      insertQuery.prepare(sql);
      insertQuery.addBindValue(sender);
      insertQuery.addBindValue(levelName);
      insertQuery.addBindValue(message);
      if(!insertQuery.exec())
        std::cerr << "QsLog: error executing query " << insertQuery.lastError().text().toUtf8().constData() << "\n";
   }

}

bool QsLogging::DatabaseDestination::isValid()
{
    return mDb.isValid() && mDb.isOpen();
}

